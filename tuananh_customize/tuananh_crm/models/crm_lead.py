# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, api, fields, _


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    # def _dupplicate_field_phone(self, cr, uid, ids, name, arg, context=None):
    #     res = {}
    #     for id in ids:
    #         record = self.browse(cr, uid, id)
    #         res.update({id: record.phone})
    #     return res
    #
    # def _dupplicate_field_email_from(self, cr, uid, ids, name, arg, context=None):
    #     res = {}
    #     for id in ids:
    #         record = self.browse(cr, uid, id)
    #         res.update({id: record.email_from})
    #     return res
    #
    # _columns = {
    #     'dupplicate_phone': fields.function(_dupplicate_field_phone, type='char', size=32, method=True, store=False,
    #                                         multi=False),
    #     'dupplicate_email_from': fields.function(_dupplicate_field_email_from, type='char', size=32, method=True,
    #                                              store=False,
    #                                              multi=False),
    # }

    #hello hehe

    contact_name = fields.Char('Contact Name', compute='_compute_partner_name', inverse='_inverse_partner_name')
    lead_branch = fields.Selection([('north', 'North'), ('South', 'South')], string='Branch', default='north')
    partner_code = fields.Char('Partner Code', compute='_compute_partner_code', inverse='_inverse_partner_code')
    partner_vat = fields.Char('Partner Vat', compute='_compute_partner_vat', inverse='_inverse_partner_vat')
    partner_street = fields.Char('Partner Address', compute='_compute_street', inverse='_inverse_street')
    partner_fax = fields.Char('Partner Fax', compute='_compute_partner_fax', inverse='_inverse_partner_fax')
    partner_alarmist = fields.Char('Partner Alarmist', compute='_compute_partner_fax', inverse='_inverse_partner_fax')
    partner_alarmist_phone = fields.Char('Partner Alarmist Phone')
    campaign_form = fields.Selection(
        [('gold', 'Gold'), ('silver', 'Silver'), ('copper', 'Copper'), ('default', 'Default')],
        string='Campaign Form', default='gold')

    @api.depends('partner_id.name')
    def _compute_partner_name(self):
        for lead in self:
            if lead.partner_id.name and lead._get_partner_name_update():
                lead.contact_name = lead.partner_id.name

    def _inverse_partner_name(self):
        for lead in self:
            if lead._get_partner_name_update():
                lead.partner_id.name = lead.contact_name

    def _get_partner_name_update(self):
        self.ensure_one()
        if self.partner_id and self.contact_name != self.partner_id.name:
            return True
        return False

    @api.depends('partner_id.customer_code')
    def _compute_partner_code(self):
        for lead in self:
            if lead.partner_id.customer_code and lead._get_partner_code_update():
                lead.partner_code = lead.partner_id.customer_code

    def _inverse_partner_code(self):
        for lead in self:
            if lead._get_partner_code_update():
                lead.partner_id.email = lead.email_from

    def _get_partner_code_update(self):
        self.ensure_one()
        if self.partner_id and self.partner_code != self.partner_id.customer_code:
            return True
        return False

    @api.depends('partner_id.vat')
    def _compute_partner_vat(self):
        for lead in self:
            if lead.partner_id.vat and lead._get_partner_vat_update():
                lead.partner_vat = lead.partner_id.vat

    def _inverse_partner_vat(self):
        for lead in self:
            if lead._get_partner_vat_update():
                lead.partner_id.vat = lead.partner_vat

    def _get_partner_vat_update(self):
        self.ensure_one()
        if self.partner_id and self.partner_vat != self.partner_id.vat:
            return True
        return False

    @api.depends('partner_id.first_address')
    def _compute_street(self):
        for lead in self:
            if lead.partner_id.first_address and lead._get_street_update():
                lead.partner_street = lead.partner_id.first_address

    def _inverse_street(self):
        for lead in self:
            if lead._get_street_update():
                lead.partner_id.first_address = lead.partner_street

    def _get_street_update(self):
        self.ensure_one()
        if self.partner_id and self.partner_street != self.partner_id.first_address:
            return True
        return False

    @api.depends('partner_id.fax')
    def _compute_partner_fax(self):
        for lead in self:
            if lead.partner_id.fax and lead._get_partner_fax_update():
                lead.partner_fax = lead.partner_id.fax

    def _inverse_partner_fax(self):
        for lead in self:
            if lead._get_partner_fax_update():
                lead.partner_id.fax = lead.partner_fax

    def _get_partner_fax_update(self):
        self.ensure_one()
        if self.partner_id and self.partner_fax != self.partner_id.fax:
            return True
        return False

    @api.depends('partner_id.alarmist')
    def _compute_partner_alarmist(self):
        for lead in self:
            if lead.partner_id.alarmist and lead._get_partner_alarmist_update():
                lead.partner_alarmist = lead.partner_id.alarmist

    def _inverse_partner_alarmist(self):
        for lead in self:
            if lead._get_partner_alarmist_update():
                lead.partner_id.alarmist = lead.partner_alarmist

    def _get_partner_alarmist_update(self):
        self.ensure_one()
        if self.partner_id and self.partner_alarmist != self.partner_id.alarmist:
            return True
        return False
