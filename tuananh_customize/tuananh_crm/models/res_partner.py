# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, api, fields, _


class ResPartner(models.Model):
    _inherit = 'res.partner'

    customer_code = fields.Char('Customer Code')
    customer_group = fields.Selection(
        [('personal', 'Personal Customer'), ('enterprise', 'Enterprise Customer'), ('foreign', 'Foreign Organization')],
        string='Customer Group', default='personal')
    fax = fields.Char('Fax')
    customer_type = fields.Selection(
        [('gold', 'Gold'), ('silver', 'Silver'), ('copper', 'Copper'), ('default', 'Default')],
        string='Customer Type', default='gold')
    alarmist = fields.Char('Alarmist')
    first_address = fields.Char('First Address')
    second_address = fields.Char('Second Address')
    bank_account_number = fields.Char('Bank Account Number')
    bank_name = fields.Char('Bank Name')
    payment_terms = fields.Char('Payment Term')
    payment_duration = fields.Char('Payment Duration')


    @api.model
    def create(self, vals):
        print(self._context)
        return super(ResPartner, self).create(vals)
