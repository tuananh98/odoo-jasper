# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Tuan Anh CRM',
    'version': '1.0',
    'category': 'Sales/CRM',
    'summary': 'Tuấn Anh đã nghịch ở đây',
    'description': "",
    'depends': ['base', 'crm'],
    'data': [
        'security/ir.model.access.csv',
        'views/crm_lead_views.xml',
        'views/res_partner.xml',
        'views/res_users.xml'
    ],
    'installable': True,
    'application': False,
    'auto_install': True,
    'license': 'LGPL-3',
}
